import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      title: 'Counter App',
      home: AppPage(),
    );
  }
}

class AppPage extends StatefulWidget {
  @override
  _AppPageState createState() => _AppPageState();
}

class _AppPageState extends State<AppPage> {
  int _count = 0;
  String _ver = '0.2b';

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Counter App'),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Text(
              '현재 카운트는 $_count회 입니다.',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          Center(
            child: CupertinoButton(
              child: Text('카운트 증가'),
              onPressed: () => setState(() => addCount()),
            ),
          ),
          Center(
            child: CupertinoButton(
              child: Text(
                '카운트 감소',
                style: TextStyle(color: CupertinoColors.destructiveRed),
              ),
              onPressed: () => setState(() {
                    if (_count > 0)
                      _count--;
                    else
                      Fluttertoast.showToast(
                          msg: '카운트를 감소할 필요가 없습니다.',
                          toastLength: Toast.LENGTH_SHORT);
                  }),
            ),
          ),
          Center(
            child: CupertinoButton(
              child: Text(
                '카운트 초기화',
                style: TextStyle(color: CupertinoColors.destructiveRed),
              ),
              onPressed: () {
                setState(() => _count = 0);
                Fluttertoast.showToast(
                    msg: '카운트가 초기화 되었습니다.', toastLength: Toast.LENGTH_SHORT);
              },
            ),
          ),
          Center(
            child: CupertinoButton(
              color: CupertinoColors.activeBlue,
              child: Text('카운트 직접 입력'),
              onPressed: () => customCount(),
            ),
          ),
          Center(
              heightFactor: 1.5,
              child: CupertinoButton(
                color: CupertinoColors.activeGreen,
                child: Text(
                  '버전 정보',
                ),
                onPressed: () => alertDialog(),
              ))
        ],
      ),
    );
  }

  void addCount() {
    if (_count == 10) {
      Fluttertoast.showToast(
          msg: '이미 카운트를 많이 누르셨습니다. 카운터를 감소시켜 보세요',
          toastLength: Toast.LENGTH_SHORT);
    } else {
      _count++;
    }
  }

  void alertDialog() {
    var alert = CupertinoAlertDialog(
      title: Text('버전 정보'),
      content: Text('현재 버전은 $_ver버전 입니다.\n이 버전에서 추가된 기능 : 버전 정보 버튼 추가'),
      actions: <Widget>[
        CupertinoDialogAction(
          isDefaultAction: true,
          child: Text('닫기'),
          onPressed: () => Navigator.pop(context),
        )
      ],
    );
    showCupertinoDialog(
      context: context,
      builder: (BuildContext context) => alert,
    );
  }

  void customCount() {
    final cont = TextEditingController();
    var alert = CupertinoAlertDialog(
      title: Text('카운트 입력'),
      content: Column(
        children: <Widget>[
          Center(
            child: Text('카운트를 입력하세요'),
          ),
          Center(
            child: CupertinoTextField(
              controller: cont,
              autofocus: false,
              clearButtonMode: OverlayVisibilityMode.editing,
              keyboardType: TextInputType.number,
            ),
          )
        ],
      ),
      actions: <Widget>[
        CupertinoDialogAction(
          isDestructiveAction: true,
          child: Text('취소'),
          onPressed: () => Navigator.pop(context),
        ),
        CupertinoDialogAction(
          isDefaultAction: true,
          child: Text('확인'),
          onPressed: () {
            if (cont.text == null) {
              Fluttertoast.showToast(
                  msg: '숫자만 입력해주세요!', toastLength: Toast.LENGTH_SHORT);
            } else {
              setState(() {
                _count = int.parse(cont.text);
                Navigator.pop(context);
              });
            }
          },
        )
      ],
    );
    showCupertinoDialog(
        context: context, builder: (BuildContext context) => alert);
  }
}
